import findIndex from "lodash/findIndex";
import filter from "lodash/filter";

let userList = [
  { id: 1, name: "aaa" },
  { id: 2, name: "bbb" },
  { id: 3, name: "ccc" },
  { id: 4, name: "ddd" },
  { id: 5, name: "eee" },
  { id: 6, name: "ggg" },
];

const getListUser = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(userList), 2000);
  });
};

const addUser = (name) => {
  return new Promise((resolve, reject) => {
    const newUser = { id: userList[userList.length - 1].id + 1, name: name };
    const userListCopy = [...userList];
    userListCopy.push(newUser);
    userList = userListCopy;
    setTimeout(() => resolve(), 1000);
  });
};

const updateUser = (user) => {
  return new Promise((resolve, reject) => {
    const userIndex = findIndex(userList, ["id", user.id]);
    const userListCopy = [...userList];
    userListCopy[userIndex] = user;
    userList = userListCopy;
    setTimeout(() => resolve(), 1000);
  });
};

const deleteUser = (user) => {
  return new Promise((resolve, reject) => {
    userList = filter(userList, (item) => user.id !== item.id);
    setTimeout(() => resolve(), 1000);
  });
};

export const userService = { getListUser, addUser, updateUser, deleteUser };
