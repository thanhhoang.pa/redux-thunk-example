import React, { useState } from "react";

function UserAdd({ addUser }) {
  const [name, setName] = useState("");

  const changeName = (event) => {
    setName(event.target.value);
  };

  const clickAddUser = () => {
    setName("");
    addUser(name);
  };

  return (
    <div>
      <span>
        Name: <input value={name} onChange={changeName} />
      </span>
      <button onClick={clickAddUser}>Add</button>
    </div>
  );
}

export default UserAdd;
