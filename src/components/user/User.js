import React, { useState } from "react";

function User({ user, updateUser, deleteUser }) {
  const [name, setName] = useState(user.name);
  const [isEdit, setIsEdit] = useState(false);

  const changeName = (event) => {
    setName(event.target.value);
  };

  const clickCancel = () => {
    setIsEdit(false);
    setName(user.name);
  };

  const clickSave = () => {
    const newUser = {
      ...user,
      name: name,
    };

    updateUser(newUser);
    setIsEdit(false);
  };

  const clickEdit = () => setIsEdit(true);
  const clickDelete = () => deleteUser(user);

  return (
    <div key={user.id}>
      <span>
        ID: {user.id}, Name:{" "}
        <input value={name} onChange={changeName} disabled={!isEdit} />
      </span>
      {isEdit ? (
        <>
          <button onClick={clickSave}>Save</button>
          <button onClick={clickCancel}>Cancel</button>
        </>
      ) : (
        <>
          <button onClick={clickEdit}>Edit</button>
          <button onClick={clickDelete}>Delete</button>
        </>
      )}
    </div>
  );
}

export default User;
