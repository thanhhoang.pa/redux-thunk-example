import React, { useEffect } from "react";
import map from "lodash/map";
import User from "../user/User";
import UserAdd from "../user-add/UserAdd";
import { useDispatch, useSelector } from "react-redux";
import {
  getListUser,
  addUser as addUserThunk,
  deleteUser as deleteUserThunk,
  updateUser as updateUserThunk,
} from "../../stores/user/userThunks";

function UserList() {
  const dispatch = useDispatch();

  const users = useSelector((state) => {
    return state.user.data;
  });

  const loading = useSelector((state) => {
    return state.user.loading;
  });

  useEffect(() => {
    dispatch(getListUser());
  }, [dispatch]);

  const addUser = async (name) => {
    await dispatch(addUserThunk(name));
    dispatch(getListUser());
  };

  const deleteUser = async (user) => {
    await dispatch(deleteUserThunk(user));
    dispatch(getListUser());
  };

  const updateUser = async (user) => {
    await dispatch(updateUserThunk(user));
    dispatch(getListUser());
  };

  return (
    <div>
      <h1>User List</h1>
      {loading ? "loading..." : ""}
      <UserAdd addUser={addUser} />
      <br />
      {map(users, (user) => (
        <User
          user={user}
          key={user.id}
          updateUser={updateUser}
          deleteUser={deleteUser}
        />
      ))}
    </div>
  );
}

export default UserList;
