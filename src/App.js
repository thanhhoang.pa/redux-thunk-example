import UserList from "./components/user-list/UserList";
import "./App.css";

function App() {
  return (
    <div className="App">
      <UserList />
    </div>
  );
}

export default App;
