import { createAsyncThunk } from "@reduxjs/toolkit";
import { userService } from "../../services/userService";

export const getListUser = createAsyncThunk("user/getListUser", async () => {
  const users = await userService.getListUser();
  return users;
});

export const addUser = createAsyncThunk("user/addUser", async (userName) => {
  const response = await userService.addUser(userName);
  return response;
});

export const updateUser = createAsyncThunk("user/updateUser", async (user) => {
  const response = await userService.updateUser(user);
  return response;
});

export const deleteUser = createAsyncThunk("user/deleteUser", async (user) => {
  const response = await userService.deleteUser(user);
  return response;
});
