import { createSlice } from "@reduxjs/toolkit";
import { getListUser, addUser, updateUser, deleteUser } from "./userThunks";

const userSlice = createSlice({
  name: "user",
  initialState: {
    data: [],
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: {
    // getListUser
    [getListUser.pending]: (state) => {
      state.loading = true;
    },
    [getListUser.rejected]: (state, action) => {
      state.loading = false;
      state.data = action.error;
    },
    [getListUser.fulfilled]: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },

    // addUser
    [addUser.rejected]: (state, action) => {
      state.loading = false;
      state.data = action.error;
    },
    [addUser.fulfilled]: (state, action) => {
      state.loading = false;
    },
    [addUser.pending]: (state) => {
      state.loading = true;
    },

    // updateUser
    [updateUser.rejected]: (state, action) => {
      state.loading = false;
      state.data = action.error;
    },
    [updateUser.fulfilled]: (state, action) => {
      state.loading = false;
    },
    [updateUser.pending]: (state) => {
      state.loading = true;
    },

    // deleteUser
    [deleteUser.rejected]: (state, action) => {
      state.loading = false;
      state.data = action.error;
    },
    [deleteUser.fulfilled]: (state, action) => {
      state.loading = false;
    },
    [deleteUser.pending]: (state) => {
      state.loading = true;
    },
  },
});

const { reducer: userReducer } = userSlice;

export default userReducer;
